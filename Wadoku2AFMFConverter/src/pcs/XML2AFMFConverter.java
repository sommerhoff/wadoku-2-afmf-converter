package pcs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;

import org.apache.xerces.parsers.SAXParser;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * This converter converts the Wadoku XML file <code>wadoku.xml</code> into AFMF (Apel's File Maker Format).
 * There are currently 2 formats, so that the possibility to convert might be necessary sometimes.
 * It's neither that user-friendly nor perfect yet :-)
 * 
 * @author Paul C. Sommerhoff
 *
 */

public class XML2AFMFConverter extends DefaultHandler implements Runnable {
	
	public static final String OUTPUTFILE = "/tmp/afmf.txt";
	public static final String IMPORT_FILENAME = "/home/pcs/wadoku/wadoku-xml-20160110/wadoku.xml";
	// process only new Wadoku entries (minimum ID > 10000000) 
	public static final int MIN_ID = 10000000; 
	
	public static final String[] PROCESSED_ELEMENTS = {
		"entry", "orth", "pron", "gramGrp", "trans", "text", "token", "pos", "sense", "usg"
	};
	
	protected StringBuffer buffer;
	protected StringBuffer output;
	protected AFMF_Entry entry;
	protected Sense sense;
	protected Usage usage;
	protected int pronAccent;
	protected String tokenGenus;
	protected int count;
	
	protected Hashtable<String, Boolean> xmlElements;
	protected String previousElement;
	
	protected boolean inOrth;
	protected boolean inIrrOrth;
	protected boolean inPron;
	protected boolean inHatsuonPron;
	protected boolean inGramGrp;
	protected boolean inTrans;
	protected boolean inText;
	protected boolean inToken;
	protected boolean inSense;
	protected boolean inUsage;
	protected boolean hasPrecedingSpace;
	protected boolean hasFollowingSpace;
	
	protected String currentElement;
	protected UnhandledElements unprocessed;
	
	protected boolean inEntity = false;
	protected String entityName = null;
	
	@Override
	public void run() {
		/*
		FileDialog fileDialog = new FileDialog(new Frame(), "Wadoku XML Import", FileDialog.LOAD);
		fileDialog.setVisible(true);
		String filename = fileDialog.getDirectory() + "/" + fileDialog.getFile();
		*/
		String filename = IMPORT_FILENAME;
		resetOutputFile();
		try {
			SAXParser parser = new SAXParser();
			parser.setContentHandler(this);
			System.out.println("Start parsing...");
			parser.parse(filename);
			System.out.println("Stopped parsing. " + count + " entries exported to " + OUTPUTFILE + ".");
		} catch(SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void startDocument() throws SAXException {
		xmlElements = new Hashtable<>();
		previousElement = "";
		buffer = new StringBuffer();
		output = new StringBuffer();
		inOrth = false;
		inIrrOrth = false;
		inPron = false;
		inHatsuonPron = false;
		inGramGrp = false;
		inTrans = false;
		inText = false;
		inToken = false;
		inSense = false;
		inUsage = false;
		hasPrecedingSpace = false;
		hasFollowingSpace = false;
		pronAccent = -1;
		count = 0;
	}

	@Override
	public void endDocument() throws SAXException {
		
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		if(!localName.equals("entry"))
			if((entry == null) || Integer.parseInt(entry.getId()) < MIN_ID)
				return;
		currentElement = localName;
		switch(localName) {
			case "entry":
				entry = new AFMF_Entry();
				entry.setId(atts.getValue("id"));
				buffer.delete(0, buffer.length());
				System.out.println("Entering entry " + entry.getId() + ".");
				if(Integer.parseInt(entry.getId()) < MIN_ID)
					System.out.println("Entry will be skipped.");
				buffer.delete(0, buffer.length());
				break;
			case "orth":
				if(atts.getLength() == 0)
					inOrth = true;
				else if(atts.getIndex("irr") != -1 && atts.getValue("irr").equals("true"))
					inIrrOrth = true;
				buffer.delete(0, buffer.length());
				break;
			case "pron":
				if(atts.getLength() == 0)
					inPron = true;
				else if(atts.getIndex("type") != -1 && atts.getValue("type").equals("hatsuon"))
					inHatsuonPron = true;
				if(atts.getIndex("accent") != -1) {
					pronAccent = Integer.parseInt(atts.getValue("accent"));
				}
				buffer.delete(0, buffer.length());
				break;
			case "gramGrp":
				inGramGrp = true;
				break;
			case "pos":
				if(atts.getLength() > 0 && atts.getIndex("type") > -1)
					entry.setGramGrpPos(atts.getValue("type"));
				break;
			case "trans":
				inTrans = true;
				buffer.delete(0, buffer.length());
				break;
			case "text":
				if(atts.getLength() > 0 && atts.getIndex("hasPrecedingSpace") != -1 && atts.getValue("hasPrecedingSpace").equals("true")) {
					hasPrecedingSpace = true;
				}
				if(atts.getLength() > 0 && atts.getIndex("hasFollowingSpace") != -1 && atts.getValue("hasFollowingSpace").equals("true")) {
					hasFollowingSpace = true;
				}
				inText = true;
				if(!(previousElement.equals("text") || previousElement.equals("token")))
					buffer.delete(0, buffer.length());
				break;
			case "token":
				inToken = true;
				tokenGenus = null;
				if(atts.getLength() > 0 && atts.getIndex("genus") != -1) {
					tokenGenus = atts.getValue("genus");
				}
				if(!(previousElement.equals("text") || previousElement.equals("token")))
					buffer.delete(0, buffer.length());
				break;
			case "sense":
				sense = new Sense();
				inSense = true;
				break;
			case "usg":
				usage = new Usage(atts.getValue("type"));
				inUsage = true;
				buffer.delete(0, buffer.length());
				break;
			default:
				if(!isInArray(PROCESSED_ELEMENTS, currentElement)) {
					buffer.delete(0, buffer.length());
					unprocessed = new UnhandledElements(currentElement);
					int numberOfAttributes = atts.getLength();
					for(int i=0; i<numberOfAttributes; i++) {
						unprocessed.addAttribute("@"+atts.getLocalName(i)+": "+atts.getValue(i)+ " ");
					}
				}
		}
		enterElement(localName);
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if(entry == null || Integer.parseInt(entry.getId()) < MIN_ID)
			return;
		previousElement = localName;
		switch(localName) {
			case "entry":
				writeEntry(entry);
				buffer.delete(0, buffer.length());
				break;
			case "orth":
				if(inOrth) {
					entry.addOrth(buffer.toString());
					inOrth = false;
				} else if(inIrrOrth) {
					entry.addIrrOrth(buffer.toString());
					inIrrOrth = false;
				}
				buffer.delete(0, buffer.length());
				break;
			case "pron":
				if(inPron) {
					entry.addPron(buffer.toString());
					inPron = false;
				} else if(inHatsuonPron) {
					entry.setHatsuonPron(buffer.toString(), pronAccent);
					inHatsuonPron = false;
				}
				buffer.delete(0, buffer.length());
				pronAccent = -1;
				break;
			case "gramGrp":
				buffer.delete(0, buffer.length());
				inGramGrp = false;
				break;
			case "trans":
				sense.addTrans(buffer.toString());
				buffer.delete(0, buffer.length());
				inTrans = false;
				break;
			case "text":
				inText = false;
				break;
			case "token":
				inToken = false;
				break;
			case "sense":
				if(usage != null) {
					sense.setUsage(usage);
				}
				entry.addSense(sense);
				usage = null;
				inSense = false;
				break;
			case "usg":
				usage.setValue(buffer.toString());
				buffer.delete(0, buffer.length());
				inUsage = false;
				break;
			default:
				if(!isInArray(PROCESSED_ELEMENTS, currentElement)) {
					unprocessed.setValue(buffer.toString());
					entry.addUnprocessed(unprocessed);
					buffer.delete(0, buffer.length());
				}
		}
		leaveElement(localName);
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if(inText || inToken || inOrth || inIrrOrth || inPron || inHatsuonPron) {
			if (inEntity) {
		        inEntity = false;
		        buffer.append("&" + entityName + ";");
		    } else {
		    	if(hasPrecedingSpace) {
		    		buffer.append(" ");
		    		hasPrecedingSpace = false;
		    	}
		    	if(currentElement.equals("token") && tokenGenus != null) {
		    		buffer.append("<HW " + tokenGenus + ": ");
		    		buffer.append(ch, start, length);
		    		buffer.append(">");
				} else {
					buffer.append(ch, start, length);
				}
				if(hasFollowingSpace) {
					buffer.append(" ");
					hasFollowingSpace = false;
				}
		    }
		} else if(inUsage) {
			buffer.append(ch, start, length);
		} else if(!isInArray(PROCESSED_ELEMENTS, currentElement)) {
			buffer.append("<" + currentElement + ": ");
			buffer.append(ch, start, length);
			buffer.append(">");
		}
	}
	
	public void writeEntry(AFMF_Entry entry) {
		try {
			System.out.println("File Output...");
			File file = new File(OUTPUTFILE); 
			FileWriter writer = new FileWriter(file, true);
			if(file.exists())
				writer.write("\n");
			writer.write(entry.toString());
			writer.close();
			count++;
		} catch (IOException e) {
			System.err.println("File Output Error: " + OUTPUTFILE);
		}
	}
	
	public void resetOutputFile() {
		File file = new File(OUTPUTFILE); 
		if(file.exists())
			file.delete();
	}
	
	public void enterElement(String element) {
		xmlElements.put(element, true);
	}
	
	public void leaveElement(String element) {
		xmlElements.put(element, false);
	}
	
	public boolean isInElement(String element) {
		if(xmlElements.containsKey(element)) {
			return xmlElements.get(element);
		}
		return false;
	}
	
	public <T> boolean isInArray(T[] array, T toSearch) {
		for(T item: array) {
			if(item.equals(toSearch)) {
				return true;
			}
		}
		return false;
	}
}
