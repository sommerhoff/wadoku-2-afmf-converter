package pcs;

/**
 * This is a helper class to construct pairs of objects.
 * 
 * @author Paul C. Sommerhoff
 *
 */

public class Pair<A, B> {
	protected A objectA;
	protected B objectB;
	
	public Pair(A objectA, B objectB) {
		this.objectA = objectA;
		this.objectB = objectB;
	}
	
	public A getObjectA() {
		return objectA;
	}
	public void setObjectA(A objectA) {
		this.objectA = objectA;
	}
	public B getObjectB() {
		return objectB;
	}
	public void setObjectB(B objectB) {
		this.objectB = objectB;
	}
	
	
}
