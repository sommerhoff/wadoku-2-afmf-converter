package pcs;

import java.util.LinkedList;

/**
 * This class handles all otherwise unhandled element types.
 * 
 * @author Paul C. Sommerhoff
 *
 */

public class UnhandledElements {
	protected String elementName;
	protected String value;
	protected LinkedList<String> attributes = new LinkedList<>(); 
	
	public UnhandledElements(String elementName) {
		this.elementName = elementName;
	}
	
	public String getElementName() {
		return elementName;
	}
	public void setElementName(String elementName) {
		this.elementName = elementName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getAttribute(int i) {
		return attributes.get(i);
	}
	public void addAttribute(String attribute) {
		attributes.add(attribute);
	}
	
	public String toString() {
		StringBuffer output = new StringBuffer();
		output.append("<"+elementName+": ");
		output.append(value);
		if(attributes.size() > 0) {
			output.append(" -- ");
		}
		for(String a: attributes) {
			output.append(a + " ");	
		}
		return output.toString();
	}
}
