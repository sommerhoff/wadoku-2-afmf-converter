package pcs;

import java.util.ArrayList;

/**
 * This class undertakes the sub task of building up AFMF etymology elements.
 *  
 * @author Paul C. Sommerhoff
 *
 */
public class Etym {
	protected ArrayList<Pair<String, String>> elements;
	
	public void add(Pair<String, String> subElement) {
		elements.add(subElement);
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("<Etym.:");
		for(Pair<String, String> element: elements) {
			sb.append(" ");
			switch(element.getObjectA()) {
				case "text":
					sb.append(element.getObjectB());
					break;
				case "foreign":
					sb.append("<For.: "+element.getObjectB()+">");
					break;
			}
		}
		sb.append(">");
		return sb.toString();
	}
}
