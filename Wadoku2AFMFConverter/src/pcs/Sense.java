package pcs;

import java.util.LinkedList;

/**
 * This class converts an XML <code>sense</code> element to an AFMF <code>MGr</code> element.
 * 
 * @author Paul C. Sommerhoff
 *
 */
public class Sense {
	protected LinkedList<String> trans = new LinkedList<>();
	protected Usage usage;
	protected Etym etym;

	public Sense() {
		trans = new LinkedList<>();
	}
	
	public String getTrans(int i) {
		return trans.get(i);
	}
	
	public void addTrans(String transText) {
		trans.add(transText);
	}
	
	public int getTransCount() {
		return trans.size();
	}
	
	public Usage getUsage() {
		return usage;
	}

	public void setUsage(Usage usage) {
		this.usage = usage;
	}
	
	public Etym getEtym() {
		return etym;
	}

	public void setEtym(Etym etym) {
		this.etym = etym;
	}
	
	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		s.append("<MGr:");
		if(usage != null) {
			s.append(" {<" + getFirstUppered(usage.getType()) + ".: " + usage.getValue() + ">}");
		}
		for(int i=0; i<trans.size(); i++) {
			String t = trans.get(i);
			s.append(getFormattedTrans(t));
		}
		if(trans.size() > 0) {
			// delete semicolon
			s.deleteCharAt(s.length()-1);
		}
		
		if(etym != null) { // TODO conditions
			s.append(" (");
		}
		
		if(etym != null) {
			s.append(" "+etym.toString());
		}
		
		if(etym != null) { // TODO conditions
			s.append(")");
		}
		
		s.append(">.");
		return s.toString();
	}
	
	public String getFormattedTrans(String t) {
		return " <TrE: " + t + ">;";
	}
	
	public String getFirstUppered(String text) {
		StringBuffer sb = new StringBuffer();
		if(text == null) {
			return null;
		}
		if(text.length() > 0) {
			sb.append(text.toUpperCase().charAt(0));
			if(text.length() > 1)
				sb.append(text.substring(1));
		}
		return sb.toString();
	}
}
