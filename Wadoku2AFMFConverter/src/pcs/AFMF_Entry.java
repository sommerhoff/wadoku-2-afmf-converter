package pcs;

import java.util.LinkedList;

/**
 * This class creates an AFMF (Apel's File Maker Format) entry.
 * 
 * @author Paul C. Sommerhoff
 *
 */
public class AFMF_Entry {
	protected String id;
	protected String hatsuonPron;
	protected int accent;
	protected String gramGrpPos;
	protected LinkedList<String> orth = new LinkedList<>();
	protected LinkedList<String> irrOrth = new LinkedList<>();
	protected LinkedList<String> pron = new LinkedList<>();
	protected LinkedList<Sense> senses = new LinkedList<>();
	
	protected LinkedList<UnhandledElements> unprocessed = new LinkedList<>();
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getHatsuonPron() {
		return hatsuonPron;
	}

	public void setHatsuonPron(String hatsuonPron, int accent) {
		this.hatsuonPron = hatsuonPron;
		this.accent = accent;
	}
	
	public String getGramGrpPos() {
		return gramGrpPos;
	}

	public void setGramGrpPos(String gramGrpPos) {
		this.gramGrpPos = gramGrpPos;
	}

	public String getOrth(int i) {
		return orth.get(i);
	}
	
	public void addOrth(String orthText) {
		orth.add(orthText);
	}
	
	public String getIrrOrth(int i) {
		return irrOrth.get(i);
	}
	
	public void addIrrOrth(String irrOrthText) {
		irrOrth.add(irrOrthText);
	}
	
	public String getPron(int i) {
		return pron.get(i);
	}
	
	public void addPron(String pronText) {
		pron.add(pronText);
	}
	
	public Sense getSense(int i) {
		return senses.get(i);
	}
	
	public void addSense(Sense sense) {
		senses.add(sense);
	}
	
	public UnhandledElements getUnprocessed(int i) {
		return unprocessed.get(i);
	}
	
	public void addUnprocessed(UnhandledElements unprText) {
		unprocessed.add(unprText);
	}

	@Override
	public String toString() {
		StringBuffer output = new StringBuffer();
		output.append(id);
		output.append("\t");
		output.append(getFormattedList(orth));
		if(irrOrth.size() > 0) 
			output.append(" (" + getFormattedList(irrOrth) + ")");
		output.append("\t");
		output.append("(<POS: " + gramGrpPos + ".>)");
		output.append(getFormattedSenses(senses));
		
		// extra fields
		output.append("\t\t" + hatsuonPron);
		output.append("\t" + accent);
		output.append("\t" + pron);
		for(UnhandledElements unprItem: unprocessed) {
			output.append("\t" + unprItem.toString());
		}
		return output.toString();
	}
	
	public String getFormattedList(LinkedList<String> list) {
		StringBuffer s = new StringBuffer();
		for(String element: list) {
			s.append(element + "; ");
		}
		if(s.length() > 1)
			s.delete(s.length()-2, s.length());
		return s.toString();
	}
	
	public String getFormattedSenses(LinkedList<Sense> senses) {
		StringBuffer s = new StringBuffer();
		for(int k=0; k<senses.size(); k++) {
			Sense sense = senses.get(k);
			s.append(" [" + (k+1) + "]"+sense);
		}
		return s.toString();
	}
}
