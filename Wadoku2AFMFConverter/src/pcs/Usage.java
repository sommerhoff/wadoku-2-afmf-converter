package pcs;

/**
 * Simple class to handle XML <code>usage</code> elements.
 * 
 * @author Paul C. Sommerhoff
 *
 */

public class Usage {
	protected String type;
	protected String value;
	
	public Usage(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
