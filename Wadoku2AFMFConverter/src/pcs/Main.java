package pcs;

/**
 * This is the main class of the project and calls the actual format converter.
 * 
 * @author Paul C. Sommerhoff
 * @version 1.0
 *
 */
public class Main {

	public static void main(String[] args) {
		System.out.println("Wadoku format converter started.");
		XML2AFMFConverter converter = new XML2AFMFConverter();
		converter.run();
		System.out.println("Wadoku format converter exited.");
	}

}
